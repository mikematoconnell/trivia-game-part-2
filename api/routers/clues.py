import numbers
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from .categories import CategoryOut, GameCategoryOut

router = APIRouter()

class ClueIn(BaseModel):
    answer: str
    question: str
    value: float
    invalid_count: int
    category: CategoryOut


class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool


class GameClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: GameCategoryOut


class Clues(BaseModel):
    page_count: int
    clues: list[ClueOut]

class Message(BaseModel):
    message: str


@router.get("/api/clues/{clue_id}", 
response_model=ClueOut, responses={404: {"model": Message}},)
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT clues.id, clues.question, clues.answer, clues.value, invalid_count, categories.title, clues.canon, categories.canon AS c_canon
                FROM clues
                JOIN categories
                    ON(clues.category_id = categories.id)
                WHERE %s = clues.id
            """,
                [clue_id],
            )
            row = cur.fetchone()
            print(row)
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return{"message": "Clue not found"}
            clue = {}
            clue_fields = [
                "id",
                "question",
                "answer",
                "value",
                "invalid_count",
                "canon"
            ]
            print()
            for i, column in enumerate(cur.description):
                if column.name in clue_fields:
                    clue[column.name] = row[i]


            category = {}
            category_fields = [
                "id",
                "title",
            ]
            for i, column in enumerate(cur.description):
                if column.name in category_fields:
                    category[column.name] = row[i]
                if column.name == "c_canon":
                    category["canon"] = row[i]
            clue["category"] = category
            return clue


@router.get("/api/random-clue", responses={404: {"model": Message}})
def get_random_clue(response: Response, invalid_count: int):
    second = 0
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT cl.id, cl.question, cl.answer, cl.value, cl.invalid_count, cat.title, cl.canon, cat.canon AS c_canon
                FROM clues AS cl
                JOIN categories AS cat
                    ON(cl.category_id = cat.id)
                WHERE cl.invalid_count = %s OR cl.invalid_count = %s
                ORDER BY RANDOM () LIMIT 1
                """,
            [invalid_count, second]
            ),

            row = cur.fetchone()
            print(row)
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return{"message": "Clue not found"}
            clue = {}
            clue_fields = [
                "id",
                "question",
                "answer",
                "value",
                "invalid_count",
                "canon"
            ]
            print(row)
            print(cur.description)
            for i, column in enumerate(cur.description):
                if column.name in clue_fields:
                    clue[column.name] = row[i]


            category = {}
            category_fields = [
                "id",
                "title",
            ]
            for i, column in enumerate(cur.description):
                if column.name in category_fields:
                    category[column.name] = row[i]
                if column.name == "c_canon":
                    category["canon"] = row[i]
            clue["category"] = category
            return clue


@router.get("/api/clues", response_model=Clues)
def get_clues(page: int = 0):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                    SELECT clues.id, clues.question, clues.answer, clues.value, clues.invalid_count, cat.title, clues.canon, cat.canon AS c_canon
                    FROM clues 
                    JOIN categories AS cat
                        ON(clues.category_id = cat.id)
                    ORDER BY clues.id
                    LIMIT 100 OFFSET %s
                """,
                [page * 100]
            ) 
            results = []
            for row in cur.fetchall():
                print("row", row)
                clue = {}
                clue_fields = [
                    "id",
                    "question",
                    "answer",
                    "value",
                    "invalid_count",
                    "canon"
                ]
                for i, column in enumerate(cur.description):
                    if column.name in clue_fields:
                        clue[column.name] = row[i]

                category = {}
                category_fields = [
                    "id",
                    "title",
                ]
                for i, column in enumerate(cur.description):
                    if column.name in category_fields:
                        category[column.name] = row[i]
                    if column.name == "c_canon":
                        category["canon"] = row[i]
                clue["category"] = category
                results.append(clue)
                print(clue)
            # don't get the purpose of this code below allows the page to be recognized otherwise it will return all instances on one page
            cur.execute(
                """
                SELECT COUNT(*) FROM clues;
                """
            )
            raw_count = cur.fetchone()[0]
            page_count = (raw_count // 100) + 1

            return Clues(page_count=page_count, clues=results)


@router.put("/api/clues/{clue_id}", response_model=ClueOut, responses={404: {"model": Message}})
def delete_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                UPDATE clues 
                SET invalid_count = %s
                WHERE clues.id = %s
                """,
                [1, clue_id]
            )
    return get_clue(clue_id, response)
