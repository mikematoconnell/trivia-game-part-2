from sqlite3 import Timestamp
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from .clues import GameClueOut
from datetime import datetime
# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()


class GameOut(BaseModel):
    id: int
    episode_id: int
    aired: str
    canon: bool
    total_amount_won: int


class CustomGame(BaseModel):
    id: int
    created_on: datetime = None
    clues: list[GameClueOut]

class Message(BaseModel):
    message: str

@router.get("/api/games/{game_id}", response_model=GameOut, responses={404: {"model": Message}})
def get_game(game_id:int, response:Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT games.id, games.episode_id, games.aired, games.canon, SUM(clues.value) as total_amount_won
                FROM games
                LEFT JOIN clues
                    ON(games.id = clues.game_id)
                WHERE games.id = %s
                GROUP BY games.id
                """,
                [game_id]
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return{"message": "Game not found"}
            record = {}
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]
            return record


@router.post("/api/custom-game", response_model=CustomGame, responses={404: {"model": Message}})
def create_game():
    with psycopg.connect(autocommit=True) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT cl.id, cl.answer, cl.question, cl.value, cl.invalid_count, cl.category_id, ct.id AS ct_id, ct.title
                FROM clues AS cl
                LEFT JOIN categories AS ct
                    ON(ct.id = cl.category_id)
                WHERE cl.canon = true
                ORDER BY RANDOM () LIMIT 30
                """
            )
            rows = cur.fetchall()
            clues = []
            for row in rows:
                clue_fields = [
                    "id",
                    "question",
                    "answer",
                    "value",
                    "invalid_count",
                    "canon"
                ]
                clue = {}
                for i, column in enumerate(cur.description):
                    if column.name in clue_fields:
                        clue[column.name] = row[i]
                category = {}
                category_fields = [
                    "id",
                    "title",
                ]
                for i, column in enumerate(cur.description):
                    if column.name in category_fields:
                        category[column.name] = row[i]
                    if column.name == "ct_id":
                        category["id"] = row[i]
                clue["category"] = category
                clues.append(clue)
            with conn.transaction():
                cur.execute(
                    """
                    INSERT INTO game_definitions (created_on)
                    VALUES (CURRENT_TIMESTAMP)
                    RETURNING id, created_on
                    """
                )
                info = cur.fetchone()
                game_id = info[0]
                created_on = info[1]
                for row in rows:
                    clue_id = row[0]
                    cur.execute(
                        """
                        INSERT INTO game_definition_clues (game_definition_id, clue_id)
                        VALUES (%s, %s)
                        """,
                        [game_id, clue_id]
                    )
                game = {}
                game["id"] = game_id
                game["created_on"] = created_on
                game["clues"] = clues
                return game
